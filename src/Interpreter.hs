import Core
import Parser
import Eval
import TypeCheck
import System.Environment(getArgs)
import System.IO

import System.Console.Haskeline

import Control.Monad.Trans.State.Strict
import Control.Monad.State (lift, when)

import Data.List
import qualified Data.Map.Lazy as Map
import Data.Either

data Command = CmdInstr Instruction
             | CmdImport String
             | CmdVar String TypeExpr
             | CmdInfo
             | CmdHelp
  deriving Show

command =
  try (fmap CmdInstr instruction)
  <|>  try (do Identifier x t <- localId; return $ CmdVar x t)
  <|> fmap CmdImport importStmt
  <|> try (do reserved ":info"; return CmdInfo)
  <|> try (do reserved ":help"; return CmdHelp)  


data InterpreterInfo = II { iiTarget :: Maybe String
                          , iiAST :: Bool
                          , iiTypeCheck :: Bool
                          , iiTCC :: TypeCheckContext
                          , iiConfig :: Config }
  deriving Show

type Interpreter = StateT InterpreterInfo IO
type Interactive = InputT Interpreter

runInterpreter :: Interpreter a -> InterpreterInfo -> IO InterpreterInfo
runInterpreter = execStateT

parseArgs :: [String] -> Interpreter Bool
parseArgs args = case args of
  [] -> return True
  "--help":_ -> do lift $ putStrLn help; return False
  "--version":_ -> do lift $ putStr $ version ++ license; return False
  "--ast":args' -> do modify $ \i -> i {iiAST = True}; parseArgs args'
  "--no-typecheck":args' -> do modify $ \i -> i {iiTypeCheck = False}; parseArgs args'
  [f] -> do modify $ \i -> i {iiTarget = Just f}; return True
  args -> error $ "Unknown arguments " ++ show args

interpreterLoop :: Interpreter ()
interpreterLoop = do
  mf <- gets iiTarget
  case mf of
    Just f -> do
      ep <- lift $ parseFromFile program f
      case ep of
        Left x -> error $ show x
        Right p -> do
          interpreterImport (programImports p)
          ast <- gets iiAST
          when ast $ lift $ print p
          tc <- gets iiTypeCheck
          tcctx <- gets iiTCC
          case runTypeChecker (tcheckProgram p) tcctx of
            Left x | tc -> error x
            _ -> do
              conf <- gets iiConfig
              lift $ runEvaluator (evalProg p) conf
              return ()
    Nothing -> do
      tcctx <- gets iiTCC
      conf <- gets iiConfig
      let tcctx' = tcctx { tcCtxThis = ["Object"]
                         , tcCtxMethod = Right $ MixinMethod ScopeNew "<default>" ["Object"] [] [] undefined }
      let conf' = conf { configEnv = Left Map.empty }
      modify $ \i -> i { iiTCC = tcctx', iiConfig = conf' }
      lift $ putStr version
      lift $ putStrLn disclaimer
      lift $ putStr tips
      runInputT defaultSettings interactiveLoop

interpreterImport :: [String] -> Interpreter ()
interpreterImport fs = aux fs []
  where
    aux [] _ = return ()
    aux (f:todo) done | f `elem` done = aux todo done
    aux (f:todo) done = do
      ep <- lift $ parseFromFile program f
      case ep of
        Left x -> error $ show x
        Right p -> do
          tcctx <- gets iiTCC
          conf <- gets iiConfig
          let tcctx' = tcctx {tcCtxDecls = nub $ tcCtxDecls tcctx ++ programMixins p}
          let conf' = conf {configDecls = nub $ configDecls conf ++ programMixins p}
          modify $ \i -> i { iiTCC = tcctx', iiConfig = conf' }
          aux (todo ++ programImports p) (done ++ [f])
          tc <- gets iiTypeCheck
          tcctx <- gets iiTCC
          case runTypeChecker (tcheckProgram p) tcctx of
            Left x | tc -> error x
            _ -> return ()

interactiveLoop :: Interactive ()
interactiveLoop = do
  line <- getInputBlock ""
  case line of
    Nothing -> liftIO $ putStrLn " Bye!"
    Just "" -> interactiveLoop
    Just line -> do
      case parse command "<stdin>" line of
        Right (CmdImport f) -> lift $ interpreterImport [f]
        Right (CmdInstr i) -> do
          ast <- lift $ gets iiAST
          when ast $ liftIO $ print i
          tc <- lift $ gets iiTypeCheck
          tcctx <- lift $ gets iiTCC
          case runTypeChecker (tcheckInstr i) tcctx of
            Left x | tc -> liftIO $ putStrLn x
            _ -> do
              conf <- lift $ gets iiConfig
              conf' <- liftIO $ runEvaluator (evalInstr i) conf
              lift $ modify $ \i -> i { iiConfig = conf' }
              return ()
        Right (CmdVar x t) -> do
          tcctx <- lift $ gets iiTCC
          conf <- lift $ gets iiConfig
          let Right met = tcCtxMethod tcctx
          let vs = methodLocals met
          if any (\(Identifier y _) -> y == x) vs
            then liftIO $ putStrLn $ "Multiple variable declaration for " ++ x
            else do
            let tcctx' = tcctx { tcCtxMethod = Right met { methodLocals = Identifier x t:vs } }
            let Left env = configEnv conf 
            let conf' = conf { configEnv = Left $ Map.insert x ObjNull env }
            lift $ modify $ \i -> i { iiTCC = tcctx', iiConfig = conf' }
        Right CmdInfo -> do
          tcctx <- lift $ gets iiTCC
          conf <- lift $ gets iiConfig
          liftIO $ print tcctx
          liftIO $ print conf
        Right CmdHelp -> liftIO $ putStr commands_help
        Left x -> liftIO $ print x
      interactiveLoop  
  where
    liftIO = lift . lift
    getInputBlock xs = do
      let style = if null xs then " >  " else " |  "
      ml <- getInputLine style
      case ml of
        Nothing ->
          if null xs
          then return ml
          else return $ Just ""
        Just line | null line -> return ml
        Just line ->
          if last line == '\\'
          then getInputBlock $ xs ++ take (length line - 1) line
          else return $ Just $ xs ++ line
      
initDecls = [Mixin "Object" [] [] [], mixinBoolean, mixinInteger, mixinString]
        
initConfig :: Config
initConfig = Config initHeap initEnv initCtx initDecls
  where
  initHeap = Map.fromList [(0,Object [] Map.empty)]
  initEnv  = Left Map.empty
  initCtx  = Top

initContext :: TypeCheckContext
initContext = TypeCheckContext initDecls [] (Left ())

main = do
  args <- getArgs
  runInterpreter (do x <- parseArgs args; when x interpreterLoop) $
    II { iiAST = False
       , iiTarget = Nothing
       , iiTypeCheck = True
       , iiTCC = initContext
       , iiConfig = initConfig }

tips = " Type :help to get a list of commands.\n"

commands_help = unlines
  [ " Interactive commands:"
  , "  :info              Print the interpreter's internal state."
  , "  :help              Print this message."
  , "  <instruction>      Run an instruction."
  , "  import <filename>  Import a file."]
       
help = "    magda [ --help | --version | --ast | --no-typecheck ]* [<filename>]\n"

version = unlines
  [ " HI Magda v1.3"
  , " An Haskell Interpreter for the Magda Language."
  , "    https://gitlab.com/magda-lang/hi-magda"
  , "  ------------------------------------------" ]

disclaimer = unlines
  [ " Haskell Interpreter for Magda  Copyright (C) 2019-2021 Magda Language"
  , " This program comes with ABSOLUTELY NO WARRANTY."
  , " This is free software, and you are welcome to redistribute it"
  , " under certain conditions. Run `magda --version` for details." ]
          
license = unlines
  [ " Haskell Interpreter for Magda"
  , " Copyright (C) 2019-2021 Magda Language"
  , " This program is free software: you can redistribute it and/or modify"
  , " it under the terms of the GNU General Public License as published by"
  , " the Free Software Foundation, either version 3 of the License, or"
  , " (at your option) any later version."
  , ""
  , " This program is distributed in the hope that it will be useful,"
  , " but WITHOUT ANY WARRANTY; without even the implied warranty of"
  , " MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the"
  , " GNU General Public License for more details."
  , " You should have received a copy of the GNU General Public License"
  , " along with this program.  If not, see <http://www.gnu.org/licenses/>."]
