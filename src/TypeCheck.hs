module TypeCheck where

import Core
import qualified Data.Map.Lazy as Map
import Data.List
import Control.Monad.State

base :: TypeExpr -> TypeChecker TypeExpr
base [] = return []
base (x:t) = do
  m <- lookupMixin x
  t' <- base $ mixinType m ++ t
  return $ nub $ x : t'

subtype :: TypeExpr -> TypeExpr -> TypeChecker Bool
subtype a ["Bottom"] = return False
subtype ["Bottom"] b = return True
subtype a b = do
  a' <- base a
  return $ all (`elem` a') b

data TypeCheckContext = TypeCheckContext
  { tcCtxDecls :: [Mixin]
  , tcCtxThis :: TypeExpr
  , tcCtxMethod :: Either () MixinMethod }

instance Show TypeCheckContext where
  show ctx = "\tDeclarations:\n\n " ++ show (tcCtxDecls ctx) ++
             "\n\n\tThis type:\n\n " ++ show (tcCtxThis ctx) ++
             "\n\n\tActual method:\n\n " ++ show (tcCtxMethod ctx)
             
type TypeChecker = StateT TypeCheckContext (Either String)

runTypeChecker :: TypeChecker a -> TypeCheckContext -> Either String TypeCheckContext
runTypeChecker = execStateT

raise :: String -> TypeChecker a
raise = lift . Left

lookupLocal :: String -> TypeChecker TypeExpr
lookupLocal x = do
  c <- get
  case tcCtxMethod c of
    Left () -> raise "Local variable reference outside any method"
    Right m ->
      let vs = methodLocals m ++ methodParams m in
      case filter (\(Identifier y yt) -> x == y) vs of
        [] -> raise $ "Local variable not declared " ++ x
        (Identifier y yt):_ -> return yt

lookupMixin :: String -> TypeChecker Mixin
lookupMixin x = do
  c <- get
  case filter (\m -> mixinName m == x) $ tcCtxDecls c of
    [] -> raise $ "Mixin reference to undeclared mixin " ++ x
    mix:_ -> return mix

lookupMixinField :: String -> String -> TypeChecker TypeExpr
lookupMixinField mix x = do
  m <- lookupMixin mix
  case filter (\f -> fieldName f == x) $ mixinFields m of
    [] -> raise $ "Mixin field reference to undeclared field " ++ x
    f:_ -> return $ fieldType f

lookupMixinMethod :: String -> String -> TypeChecker MixinMethod
lookupMixinMethod mix x = do
  m <- lookupMixin mix
  case filter (\met -> methodName met == x) $ mixinMethods m of
    [] -> raise $ "Mixin method reference to undeclared method " ++ x
    met:_ -> return met
    
tcheckMany :: [TypeChecker a] -> TypeChecker ()
tcheckMany = sequence_

tcheckProgram :: Program -> TypeChecker ()
tcheckProgram p = do
  c <- get
  let mixs = nub $ tcCtxDecls c ++ programMixins p
  put c { tcCtxDecls = mixs }
  tcheckMany $ map tcheckMixin mixs
  tcheckInstr $ programMain p

tcheckMixin :: Mixin -> TypeChecker ()
tcheckMixin m = do
  c <- get
  put c { tcCtxThis = mixinName m : mixinType m }
  tcheckMany $ map tcheckMethod $ mixinMethods m

tcheckMethod :: MixinMethod -> TypeChecker ()
tcheckMethod m = do
  c <- get
  put c { tcCtxMethod = Right m }
  tcheckInstr $ methodBody m

tcheckInstr :: Instruction -> TypeChecker ()

tcheckInstr (AssignVar x e) = do
  et <- tcheckExpr e
  xt <- lookupLocal x
  sub <- et `subtype` xt
  if sub
    then return ()
    else raise "Unmatching types for variable assignment"

tcheckInstr (AssignField e1 mix f e2) = do
  e1t <- tcheckExpr e1
  sub <- e1t `subtype` [mix]
  if sub
    then do
      ft <- lookupMixinField mix f
      e2t <- tcheckExpr e2
      sub <- e2t `subtype` ft
      if sub
        then return ()
        else raise "Unmatching types for field assignment"
    else raise $ "Unmatching mixin dereference " ++ mix ++ "." ++ f ++ " for the type " ++ show e1t
    
tcheckInstr (Return e) = do
  et <- tcheckExpr e
  c <- get
  case tcCtxMethod c of
    Left () -> raise "Return instruction outside method declaration"
    Right m -> do
      sub <- et `subtype` methodType m
      if sub
        then return ()
        else raise "Wrong return type"
    
tcheckInstr (While e i) = do
  et <- tcheckExpr e
  sub <- et `subtype` ["Boolean"]
  if sub
    then tcheckInstr i
    else raise $ "Non boolean expression: " ++ show e

tcheckInstr (If e i1 i2) = do
  et <- tcheckExpr e
  sub <- et `subtype` ["Boolean"]
  if sub
    then do tcheckInstr i1
            tcheckInstr i2
    else raise $ "Non boolean expression: " ++ show e

tcheckInstr (Cons i1 i2) = do
  tcheckInstr i1
  tcheckInstr i2

tcheckInstr (IE e) = do
  tcheckExpr e
  return ()

tcheckInstr (NativeIO f) = return ()

tcheckParams :: [Expression] -> [TypeExpr] -> TypeChecker ()
tcheckParams [] [] = return ()
tcheckParams (p:ps) (t:ts) = do
  pt <- tcheckExpr p
  sub <- pt `subtype` t
  if sub
    then tcheckParams ps ts
    else raise "Wrong parameter type"
tcheckParams _ _ = raise "Wrong parameters number"

tcheckExpr :: Expression -> TypeChecker TypeExpr
tcheckExpr (ObjRef v) = case v of
  ObjNull -> return ["Bottom"]
  ObjBool _ -> return ["Object", "Boolean"]
  ObjInt _ -> return ["Object", "Integer"]
  ObjString _ -> return ["Object", "String"]
  ObjThis -> gets tcCtxThis
  ObjMixin _ -> undefined

tcheckExpr (ExprId x) = lookupLocal x

tcheckExpr (ExprField e mix f) = do
  et <- tcheckExpr e
  sub <- et `subtype` [mix]
  if sub
    then lookupMixinField mix f
    else raise "Invalid mixin field dereference"

tcheckExpr (ExprCall e mix met params) = do
  et <- tcheckExpr e
  sub <- et `subtype` [mix]
  if sub
    then do
      m <- lookupMixinMethod mix met
      let pst = map (\(Identifier x xt) -> xt) (methodParams m)
      tcheckParams params pst
      return $ methodType m
    else raise $ "Invalid mixin method dereference " ++ mix ++ "." ++ met ++ " type is " ++ show et

tcheckExpr (SuperCall params) = do
  c <- get
  case tcCtxMethod c of
    Left () -> raise "Illegal super call: outside method definition"
    Right m ->
      if methodScope m == ScopeOver
        then do
          let pst = map (\(Identifier x xt) -> xt) (methodParams m)
          tcheckParams params pst
          return $ methodType m
        else
          raise "Illegal super call: outside override method"
    
tcheckExpr (ExprNew t) = do
  bt <- base t
  unless (all (`elem` "Object":t) bt) $
    raise $ "Incomplete base in new expression, expected: " ++ show bt ++ " found: " ++ show ("Object":t)
  return t

tcheckExpr (ExprIs e1 e2) = do
  tcheckExpr e1
  tcheckExpr e2
  return ["Object", "Boolean"]
