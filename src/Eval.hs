module Eval where

import Core
import Control.Monad.State

import Data.List
import qualified Data.Map.Lazy as Map


--Evaluator utilities

type Eval = StateT Config IO

runEvaluator :: Eval a -> Config -> IO Config
runEvaluator = execStateT

lookupHeap :: Value -> Eval (Maybe Object)
lookupHeap (ObjMixin addr) = do
  h <- gets configHeap
  return $ Map.lookup addr h

lookupHeap ObjNull = return Nothing
lookupHeap (ObjBool _) = return $ Just instanceBoolean
lookupHeap (ObjInt _) = return $ Just instanceInteger
lookupHeap (ObjString _) = return $ Just instanceString

--Programs

evalProg :: Program -> Eval ()
evalProg p = do --Note that import evaluation is done as a preprocessing step
  c <- get
  put $ c {configDecls = nub $ configDecls c ++ programMixins p}
  evalInstr $ programMain p

--Instructions

evalInstr :: Instruction -> Eval ()

evalInstr (AssignVar var e) = do
  val <- evalExpr e
  c <- get
  let Left env = configEnv c
  put $ c {configEnv = Left $ Map.insert var val env}
  
evalInstr (AssignField e mixin field e') = do
  val <- evalExpr e
  val' <- evalExpr e'
  
  mobj <- lookupHeap val
  let Just obj = mobj 
  let obj' = obj {objFields = Map.insert (mixin,field) val' (objFields obj)}

  c <- get
  let ObjMixin addr = val
  put $ c {configHeap = Map.insert addr obj' $ configHeap c}
  
evalInstr (Return e) = do
  val <- evalExpr e
  c <- get
  put $ c {configEnv = Right val}

evalInstr i'@(While e i) = do
  val <- evalExpr e
  let ObjBool b = val
  when b $
    evalInstr (Cons i i')
  
evalInstr (If e i1 i2) = do
  val <- evalExpr e
  let ObjBool b = val
  if b
    then evalInstr i1
    else evalInstr i2

evalInstr (Cons i1 i2) = do
  evalInstr i1
  c <- get
  case configEnv c of
    Right _ -> return ()
    Left _  -> evalInstr i2
  
evalInstr (IE e) = do
  evalExpr e
  return ()

evalInstr (NativeIO f) = do
  c <- get
  c' <- lift $ f c
  put c'

-- method call facilities
initLocals :: MixinMethod -> [Value] -> Environment -> Environment
initLocals met vs (Left env) =
  let actuals = zip (map idName (methodParams met)) vs in
    Left $ Map.union (Map.fromList actuals) env 
    
initEnv :: MixinMethod -> Environment
initEnv met =
  let localIds = (methodParams met ++ methodLocals met) in
    Left $ Map.fromList $ map (\x -> (idName x, ObjNull)) localIds

evalParams :: [Expression] -> Eval [Value]
evalParams = mapM evalExpr

--Expressions
evalExpr :: Expression -> Eval Value

evalExpr (ObjRef val) =
  case val of
    ObjThis -> gets $ ctxThis.configCtx
    _ -> return val

evalExpr (ExprId x) = do
  c <- get
  let Left env = configEnv c
  let Just v = Map.lookup x env
  return v

evalExpr (ExprField e mixin field) = do
  mobj <- evalExpr e >>= lookupHeap
  let Just obj = mobj
  let Just val = Map.lookup (mixin,field) (objFields obj)
  return val  

evalExpr (ExprCall e mixin method params) = do
  -- target evaluation
  val <- evalExpr e
  mobj <- lookupHeap val
  let Just obj = mobj
      m = bindMethod (objMixins obj) (mixin,method)
  --parameters evaluation
  actuals <- evalParams params
  --body execution
  c <- get
  let env = initLocals m actuals (initEnv m)
      ctx = Context val (mixin,method)
  put $ c {configEnv = env, configCtx = ctx}
  evalInstr $ methodBody m
  --heap update
  c' <- get
  put $ c {configHeap = configHeap c'}
  --retreive return value 
  let Right retval = configEnv c'
  return retval
  where
    bindMethod :: [Mixin] -> (String,String) -> MixinMethod
    bindMethod mixs (mixin,method) =
      head $ [ met | mix <- mixs
                   , met <- mixinMethods mix
                   , mixinName mix == mixin
                   , methodName met == method ]

evalExpr (SuperCall params) = do
  -- parameters evaluation
  actuals <- evalParams params    
  -- retreive target
  ctx <- gets configCtx
  Just obj <- lookupHeap $ ctxThis ctx
  let thisMet = ctxMet ctx
      (mix,met) = bindSuper (objMixins obj) (ctxMet ctx)
  -- body execution
  c <- get
  let env = initLocals met actuals (initEnv met)
      ctx' = Context (ctxThis ctx) (ctxMet ctx)
  put $ c {configEnv = env, configCtx = ctx'}
  evalInstr $ methodBody met
  -- update heap
  c' <- get
  put $ c {configHeap = configHeap c'}
  -- extract return value
  let Right retval = configEnv c'
  return retval
  where    
    bindSuper :: [Mixin] -> (String,String) -> (String,MixinMethod)
    bindSuper mixs (mixin,method) =
     let ms = [ (mixinName mix, met) | mix <- mixs
                                     , met <- mixinMethods mix
                                     , methodName met == method ] in
       fst $ head $ filter (\(_,(name,_)) -> name == mixin) $ zip ms $ tail ms
    
evalExpr (ExprNew types) = do
  c <- get  
  obj <- emptyObject types
  let h = configHeap c
  let addr = fst $ Map.findMax h
  put $ c {configHeap = Map.insert (addr + 1) obj h}
  return $ ObjMixin (addr + 1)
  where
    emptyObject :: TypeExpr -> Eval Object
    emptyObject te = do
      mixs <- bindMixins te 
      return $ Object mixs (createFields mixs)

    bindMixins :: TypeExpr -> Eval [Mixin]
    bindMixins ns = do
      ms <- gets configDecls
      return $ filter (\m -> mixinName m `elem` ns) ms

    createFields :: [Mixin] -> Map.Map (String,String) Value
    createFields ms = Map.fromList $
      [((mixinName m,fieldName f),ObjNull) | m <- ms, f <- mixinFields m]
        
evalExpr (ExprIs e1 e2) = do
  v1 <- evalExpr e1
  v2 <- evalExpr e2
  return $ ObjBool $ v1==v2

--Native mixins and instances definitions

nativeMethod name ret params locals code =
  MixinMethod ScopeNew name ret params locals code

--Object

--Boolean
mixinBoolean :: Mixin
mixinBoolean = Mixin "Boolean" [] [] [metPrint, metNot, metAnd, metOr]
  where
    boolId x = Identifier x ["Boolean"]

    metPrint = nativeMethod "print" ["Object"] [] [] (NativeIO natPrint)
    natPrint c = do
      (ObjBool x,c') <- runStateT (evalExpr $ ObjRef ObjThis) c
      print x
      (_,c'') <- runStateT (evalInstr $ Return $ ObjRef ObjNull) c'
      return c''

    metNot = nativeMethod "not" ["Boolean"] [] [] natNot
    natNot = Return $ ExprIs (ObjRef ObjThis) (ObjRef $ ObjBool False)

    metAnd = nativeMethod "and" ["Boolean"] [boolId "b"] [] natAnd
    natAnd = If (ExprId "b")
                (If (ObjRef ObjThis)
                    (Return $ ObjRef $ ObjBool True)
                    (Return $ ObjRef $ ObjBool False) )
                (Return $ ObjRef $ ObjBool False)

    metOr = nativeMethod "or" ["Boolean"] [boolId "b"] [] natOr
    natOr = If (ExprId "b")
               (Return $ ObjRef $ ObjBool True)
               (If (ObjRef ObjThis)
                   (Return $ ObjRef $ ObjBool True)
                   (Return $ ObjRef $ ObjBool False) )    

instanceBoolean :: Object
instanceBoolean = Object [mixinBoolean] Map.empty

--Integer
mixinInteger :: Mixin
mixinInteger = Mixin "Integer" [] [] [metPrint,metAdd,metGt]
  where
    intId x = Identifier x ["Integer"]
    boolId x = Identifier x ["Boolean"]    

    metPrint = nativeMethod "print" ["Object"] [] [] (NativeIO natPrint)
    natPrint c = do
      (ObjInt x,c') <- runStateT (evalExpr $ ObjRef ObjThis) c
      print x
      (_,c'') <- runStateT (evalInstr $ Return $ ObjRef ObjNull) c'
      return c''

    metAdd = nativeMethod "add" ["Integer"] [intId "n"] [] (NativeIO natAdd)
    natAdd c = do
      (ObjInt x,c') <- runStateT (evalExpr $ ObjRef ObjThis) c            
      (ObjInt y,c'') <- runStateT (evalExpr $ ExprId "n") c'          
      (_,c''') <- runStateT (evalInstr $ Return $ ObjRef $ ObjInt $ x+y) c''
      return c'''

    metGt = nativeMethod "gt" ["Boolean"] [intId "n"] [] (NativeIO natGt)
    natGt c = do
      (ObjInt x,c') <- runStateT (evalExpr $ ObjRef ObjThis) c            
      (ObjInt y,c'') <- runStateT (evalExpr $ ExprId "n") c'          
      (_,c''') <- runStateT (evalInstr $ Return $ ObjRef $ ObjBool $ x>y) c''
      return c'''

instanceInteger :: Object
instanceInteger = Object [mixinInteger] Map.empty

--String
mixinString :: Mixin
mixinString = Mixin "String" [] [] [metPrint,metAppend]
  where
    strId x = Identifier x ["String"]

    metPrint = nativeMethod "print" ["Object"] [] [] (NativeIO natPrint)
    natPrint c = do
      (ObjString x,c') <- runStateT (evalExpr $ ObjRef ObjThis) c
      putStrLn x
      (_,c'') <- runStateT (evalInstr $ Return $ ObjRef ObjNull) c'
      return c''

    metAppend = nativeMethod "append" ["String"] [strId "s"] [] (NativeIO natAppend)
    natAppend c = do
      (ObjString x,c') <- runStateT (evalExpr $ ObjRef ObjThis) c            
      (ObjString y,c'') <- runStateT (evalExpr $ ExprId "s") c'          
      (_,c''') <- runStateT (evalInstr $ Return $ ObjRef $ ObjString $ x++y) c''
      return c'''

instanceString :: Object
instanceString = Object [mixinString] Map.empty
