# Contribute to HI Magda

In order to contribute to this project you shoud follow these steps:

1. clone this repository locally

```{.sh}
$ git clone https://gitlab.com/magda-lang/hi-magda.git
```

2. fork the develop branch into a new branch, the name should make
   clear the purpose of the contribution (eg. bugfix-typechecker,
   feature-inimods)
   
```{.sh}
$ git checkout develop
$ git checkout -b my-contribution
```
   
3. commit your changes to the new branch

4. create a [merge
   request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#new-merge-request-from-your-local-environment)
   to the upstream develop branch

```{.sh}
$ git push origin develop
```

5. your changes will eventually be reviewed by a developer and merged
   into the upstream develop branch
